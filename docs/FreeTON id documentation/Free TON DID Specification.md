# Free TON DID Specification

# Table of contents

# Key terms (according to [did-core](https://www.w3.org/TR/did-core))

**Decentralized identifiers (DIDs)** are globally unique persistent identifiers that do not require a centralized registration authority and are often generated and/or registered cryptographically.

**DID controller** of a DID is the entity (person, organization, or autonomous software) that has the capability to make changes to a [](https://www.w3.org/TR/did-core/#dfn-did-documents)DID Document. 

**DID document** is a document containing information associated with a DID. They typically express verification methods, such as cryptographic public keys, and services relevant to interactions with the DID subject.

**DID method** is the mechanism by which a particular type of [DID](https://www.w3.org/TR/did-core/#dfn-decentralized-identifiers) and its associated DID document is created, resolved, updated, and deactivated.

**DID subject** is the entity identified by a DID and described by a DID document.

**Verifiable data registry** is a system or network to which DIDs are recorded to be resolvable to DID Documents.

# Abstract

Decentralized identifiers are a new type of identifier that may be decoupled from any type of centralized registries and certificate authorities. DIDs designed in order to enable the proving of control without requiring permission from any third party with the help of cryptography. Each DID associates its subject with a DID Document which contains the data (verification methods, services, etc.) that enables trustable interactions with this subject.

**FreeTON.id** employs a DID method that is implemented natively on **Free TON network**. It uses the Free TON smart contracts to store information that makes up the DID document for the FreeTON.id.

The name string that shall identify this DID method is **"freeton".**

# DID Syntax

The generic DID scheme is a URI scheme conformant with RFC3986.

According to the W3C's Decentralized Identifiers (DIDs) v1.0 data model, a Decentralized Identifier is a URI composed of three parts: the scheme (**did:**), the method identifier (**freeton**), and a unique, method-specific identifier specified by the DID method

Free TON DID method corresponds to requirements laid down in [section 8.1](https://www.w3.org/TR/did-core/#method-syntax) of did-core.

A DID that uses this method MUST begin with the following prefix: **did:freeton**. Per the DID specification, this string MUST be in lowercase. The remainder of the DID, after the prefix, is a **method-specific identifier,** which is formed on the basis of user's public key.

```bash
**did**:**freeton**:<**method-specific-identifier**>
```

## Generic relations scheme

![Untitled](/img/docs/Free TON DID Specification/Untitled.png)

# CRUD Operation Definitions

The DID Storage smart contract serves as a primary interface through all the DID management operations are performed. Therefore, all the CRUD operations should be done with corresponding contract methods calls either directly or via our DID Storage SDK.

## Create

A Free TON ID is created by deploying DID Storage Contract to Free TON blockchain and calling contract methods for DID meta document creation. The meta document takes a ton wallet address as the controller. The controller is the DID which is allowed to update the DID Document info.

To created DID, the **addDid** method in DID Storage with suitable arguments should be called.

```graphql
name: 'addDid',
        inputs: [
            {name: 'pubKey', type: 'uint256'},
            {name: 'didDocument', type: 'string'}
        ],
        outputs: [
            {name: 'value0', type: 'uint256'}
        ]
```

The result of the transaction is a DID record in the DID Storage:

```json
{
  "id": 74,
  "doc_hash": "f34a776c1e77f8d046cc02be9a15d419b829f0cbb20c23a274012e7f86ea0324",
  "issuer": "0:d0936a9fc29b5175487208b1d07ab8042ce7ddbc2de7e271c4087ca833b865cc",
  "data": {
    "status": "active",
    "expired_at": "1628598792"
  }
}
```

DID Document example in DID Storage

```json
{
  "id": "978cae5ccb0048de4bf6c76ffba5c2686987fd72494137de8373a84e5f720063",
  "createdAt": "1633219196333",
  "@context": [
      "https://www.w3.org/ns/did/v1",
      "https://w3id.org/security/suites/ed25519-2020/v1"
  ],
  "publicKey": "978cae5ccb0048de4bf6c76ffba5c2686987fd72494137de8373a84e5f720063"
}
```

**DID Document fields**

- **@context:** reference to necessary JSON-LD contexts.
- **ID**: Reference to DID bound to DID Document
- **createdAt**: DID Document creation date (Unix date, GMT)
- **publicKey:** DID controller's public key

## Read

To load DID Document first take DID and via DID Storage load DID Document from Free TON blockchain.

```graphql
{
        name: 'getDid',
        inputs: [
            {name: 'id', type: 'uint256'}
        ],
        outputs: [
            {components: [{name: 'status', type: 'string'}, {name: 'issuerPubKey', type: 'uint256'}, {name: 'didDocument', type: 'string'}], name: 'value0', type: 'tuple'}
        ]
},
```

## Update and Deactivate

Updating and deactivating the DID Document should be available through calling DID Storage smart contract methods by the owner of the corresponding DID / DID Document. This function is designed for DID / DID Document management. 

# Security Requirements

Free TON ID derives most of its security properties from the Free TON blockchain. Most notably censorship resistance, decentralization, and requiring a minimal amount of data to be synced to completely verify the integrity of a Free TON ID. We also took into consideration the requirements laid down in [Section 8.3](https://www.w3.org/TR/did-core/#security-requirements) of did-core. 

# Privacy Requirements

See [§ 10. Privacy Considerations](https://www.w3.org/TR/did-core/#privacy-considerations) in did-core.

# Extensibility

The Free TON ID Method could also easily be extended to support other features specified in [did-core](https://w3c.github.io/did-core/) (e.g endpoints, verification methods, etc.)
